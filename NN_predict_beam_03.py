# loads a model generated with model_dense_tsp_04.py (filename_model)
# and applies it on some test-data (filename_raw)
# it displays then the application of the model and beamsearch:
#   original path, predited path and edge-probabilities
# parameters: for beamsearch, see thesis or play with them


import numpy as np
from keras.models import load_model
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.colors as mcolors
import matplotlib
matplotlib.use("TkAgg")

from functions_lib import beamsearch, pathlen


#np.set_printoptions(linewidth=180, precision=4)
np.set_printoptions(formatter={'float': '{:.4f}'.format})
seed = np.random.randint(999999)
rng = np.random.default_rng(seed=seed)
#################################################################################
#filename_load = "../data/tsp_solved_n100x10x10_000_BGR192_t1p5b0LTA.npz"
#filename_raw =  "../data/tsp_solved_n100x10x10_000.npz"  # use SAME data as in load!!!

#filename_load = "../data/tsp_solved_n1000x10x10_000_BGR192_t1p5b0LTA.npz"
filename_raw =  "../data/tsp_solved_n100x12x12_000.npz"  # use SAME data as in load!!!

filename_model = "models/modelNN_tsp_solved_n10000x12x12_000_017.h5"

# beam parameters
beam_start = 3
beam_min = 2
beam_decay = .5
verbose = 1

disp_speed = 1  # ms, -2 for no display, add save results!!!, -1 for only final result, 0 for debug each step

dispsize = [5 * 280, 280]
threshold = 192  # for old mask
pt_size = 3

# load data
print(f"data {filename_raw}")
data = np.load(filename_raw)
# print(list(data.keys()))

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

n_points = data_co.shape[1]
#img_dim = int(filename_load.split("_")[4][3:])

print(f"RAW: data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

print(f"loading {filename_model}")

###################### change to NN

print(f"data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

X_dim = data_dm.shape[-1]
print(f"X_dim: {X_dim}")
X0 = data_dm[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]
y0 = data_y[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]  ## WTF!!!

print(f"shapes: X0: {X0.shape} ({X0.dtype})")

model = load_model(filename_model)
#print(model.summary())

y_pred_0 = model.predict(X0)
print(f"y_pred.shape {y_pred_0.shape}")

y_pred = np.zeros(shape=(data_dm.shape), dtype=np.float_)

m0 = np.mean(y_pred_0[y0 == 0])
m1 = np.mean(y_pred_0[y0 == 1])
print(f"look at means from pred: m0: {m0:.2f}, m1: {m1:.2f}")


# display stuff:
c_lin = "#333333"
c_lin2 = "#ff3311"
s_dot = 50

fig, ax = plt.subplots(1, 3)
ax[0].set_xlim(0, 1)
ax[0].set_ylim(0, 1)
ax[1].set_xlim(0, 1)
ax[1].set_ylim(0, 1)
ax[2].set_xlim(0, 1)
ax[2].set_ylim(0, 1)

#for i in rng.choice(X0.shape[0], size=100, replace=False):
#for i in range(X0.shape[0]):
for i in range(16, 100):
    y_pred[i, :][np.tril_indices(data_dm.shape[-1], k=-1)] = y_pred_0[i, :]
    y_pred[i, :] = y_pred[i, :] + y_pred[i, :].T  # this mirrors tril to triu - no need for beam (???)
    #print(f"y_pred[i, :]\n{y_pred[i, :]}")  # probability matrix

    beam_path = beamsearch(y_pred[i, :], start=-1, startwith=None, beam_start=beam_start, beam_min=beam_min, beam_decay=beam_decay, verbose=verbose)
    dm = data_dm[i, :]
    plall = pathlen(beam_path, dm)
    best_ix = np.argsort(plall)
    path_ix = 0

    print(f"o_len: {data_pl[i]:.4f} best_ix {plall[best_ix[path_ix]]:.4f} "
          f"incr: {100 * (plall[best_ix[path_ix]] / data_pl[i] - 1):.2f}% "
          f"beams: {beam_path.shape[0]} --- best pred path: {beam_path[best_ix[0], :]}")

    ax[0].plot((data_co[i, data_pa[i, :-1], 0], data_co[i, data_pa[i, 1:], 0]),
               (data_co[i, data_pa[i, :-1], 1], data_co[i, data_pa[i, 1:], 1]), c=c_lin, zorder=1)
    ax[0].plot((data_co[i, data_pa[i, -1], 0], data_co[i, data_pa[i, 0], 0]),
               (data_co[i, data_pa[i, -1], 1], data_co[i, data_pa[i, 0], 1]), c=c_lin, zorder=1)

    ax[1].plot((data_co[i, beam_path[best_ix[path_ix]][:-1], 0], data_co[i, beam_path[best_ix[path_ix]][1:], 0]),
               (data_co[i, beam_path[best_ix[path_ix]][:-1], 1], data_co[i, beam_path[best_ix[path_ix]][1:], 1]), c=c_lin, zorder=1)
    ax[1].plot((data_co[i, beam_path[best_ix[path_ix]][-1], 0], data_co[i, beam_path[best_ix[path_ix]][0], 0]),
               (data_co[i, beam_path[best_ix[path_ix]][-1], 1], data_co[i, beam_path[best_ix[path_ix]][0], 1]), c=c_lin, zorder=1)

    # plot Kuratowsky with probabilities
    prob_pred = 0
    for i1 in range(X_dim):
        for i2 in range(i1, X_dim):
            intensity = y_pred[i, i1, i2] ** 2
            prob_pred += y_pred[i, i1, i2]
            rgb = (1, 1 - intensity, 1 - intensity)
            color = mcolors.to_hex(rgb)
            ax[2].plot([data_co[i, i1, 0], data_co[i, i2, 0]], [data_co[i, i1, 1], data_co[i, i2, 1]], linewidth=3, color=color, zorder=1)

    prob_pred /= (X_dim * (X_dim - 1) * .5)

    # add circles around points
    for coord in data_co[i, :]:
        circle0 = patches.Circle(coord, .01, edgecolor=(0.1, 0.8, 0.6), linewidth=5, facecolor="none", zorder=0)
        ax[0].add_patch(circle0)
        circle1 = patches.Circle(coord, .01, edgecolor=(0.1, 0.8, 0.6), linewidth=5, facecolor="none", zorder=0) # yesyes...copy...
        ax[1].add_patch(circle1)
        circle2 = patches.Circle(coord, .01, edgecolor=(0.1, 0.8, 0.6), linewidth=5, facecolor="none", zorder=2)
        ax[2].add_patch(circle2)

    ax[0].set_title(f"Scenario {i} - orig len: {data_pl[i]:.4f}")
    ax[1].set_title(f"beam len: {plall[best_ix[path_ix]]:.4f} +{100 * (plall[best_ix[path_ix]] / data_pl[i] - 1):.2f}%")
    ax[2].set_title(f"prob: pred: {prob_pred:.4f}, tot: {np.mean(y_pred[i, :]):.4f} (averages)")


    key_event = plt.waitforbuttonpress()
    ax[0].cla()
    ax[1].cla()
    ax[2].cla()
