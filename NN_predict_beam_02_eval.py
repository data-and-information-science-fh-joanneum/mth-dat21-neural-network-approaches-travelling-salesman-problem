# as NN_predict_beam_x, but saves all predictions in filename_result (name generated from model- and data-name)
# filename_model_list can contain more than one input file, to calculate multiple models
# but all based on the same evaluation data (filename_raw)


import numpy as np
import os
from keras.models import load_model
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")

from functions_lib import beamsearch, pathlen


#np.set_printoptions(linewidth=180, precision=4)
np.set_printoptions(formatter={'float': '{:.2f}'.format})
seed = np.random.randint(999999)
rng = np.random.default_rng(seed=seed)
#################################################################################
filename_raw =  "../data/tsp_solved_n100x12x12_000.npz"
model_dir = "models/"
filename_model_list = []  # add as many as you like or leave empty for all models in model_dir
#filename_model_list = ["models/modelNN_tsp_solved_n10000x12x12_000_018.h5",
#                       "models/modelNN_tsp_solved_n10000x12x12_000_019.h5"]
filename_model_list = ["models/modelNN_tsp_solved_n1000x12x12_000_000.h5"]
                       #"models/modelNN_tsp_solved_n10000x8x8_000_000.h5",
                       #"models/modelNN_tsp_solved_n10000x12x12_000_020_e500.h5",
                       #"models/modelNN_tsp_solved_n10000x20x20_000_000_d0005.h5"]


# or go for the complete model-dir (friendly advise: go greedy (1,1,x) or overnight ;-))
if len(filename_model_list) == 0:

    for filename in os.listdir(model_dir):
        if os.path.isfile(os.path.join(model_dir, filename)):
            if "10000" in filename:
                filename_model_list.append(os.path.join(model_dir, filename))
                print(len(filename), os.path.join(model_dir, filename))

#print(filename_model_list)
#quit()
# beam parameters
beam_start = 1
beam_min = 1
beam_decay = .5
verbose = 0  # -1: nothing, 0: only progress, is passed through beamsearch

dosave = False


# load data
print(f"data {filename_raw}")
data = np.load(filename_raw)
# print(list(data.keys()))

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

n_points = data_co.shape[1]
#img_dim = int(filename_load.split("_")[4][3:])

print(f"RAW: data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")
###################### change to NN

print(f"data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

X_dim = data_dm.shape[-1]
print(f"X_dim: {X_dim}")
X0 = data_dm[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]
y0 = data_y[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]  ## WTF!!!

print(f"shapes: X0: {X0.shape} ({X0.dtype})")


for filename_model in filename_model_list:
    #filename_result = "../results/res_" + "3_2_50_" + filename_model.split("/")[-1].split(".")[0] + ".npz"
    filename_result = "../results/res_" + \
                      f"{beam_start}_{beam_min}_{beam_decay:.2f}_{filename_model.split('/')[-1].split('.')[0]}.npz".replace(".", "", 1)
    print(f"loading {filename_model} - processing {filename_result}")


    model = load_model(filename_model)
    y_pred_0 = model.predict(X0)
    #print(f"y_pred.shape {y_pred_0.shape}")

    y_pred = np.zeros(shape=(data_dm.shape), dtype=np.float_)

    m0 = np.mean(y_pred_0[y0 == 0])
    m1 = np.mean(y_pred_0[y0 == 1])
    print(f"look at means from pred: m0: {m0:.2f}, m1: {m1:.2f}")

    n_res = X0.shape[0]
    results = np.empty(shape=(n_res, 2), dtype=np.float_)

    for i in range(n_res):
        y_pred[i, :][np.tril_indices(data_dm.shape[-1], k=-1)] = y_pred_0[i, :]
        y_pred[i, :] = y_pred[i, :] + y_pred[i, :].T  # this mirrors tril to triu - no need for beam (???)
        #print(y_pred[i, :])

        beam_path = beamsearch(y_pred[i, :], start=-1, startwith=None, beam_start=beam_start, beam_min=beam_min, beam_decay=beam_decay, verbose=verbose)
        dm = data_dm[i, :]
        plall = pathlen(beam_path, dm)
        best_ix = np.argsort(plall)
        path_ix = 0

        #print(f"o_len: {data_pl[i]:.4f} best_ix {plall[best_ix[path_ix]]:.4f} "
        #      f"incr: {100 * (plall[best_ix[path_ix]] / data_pl[i] - 1):.2f}% "
        #      f"beams: {beam_path.shape[0]} --- best pred path: {beam_path[best_ix[0], :]}")

        results[i, 0] = data_pl[i]
        results[i, 1] = plall[best_ix[path_ix]]
        if verbose >= 0:
            print(f"i: {i}/{n_res}, {results[i, :]} - {filename_model}")

        #print(f"i: {i}, ")


    if dosave:
        print(f"saving results in {filename_result}")
        np.savez_compressed(filename_result, results=results)
