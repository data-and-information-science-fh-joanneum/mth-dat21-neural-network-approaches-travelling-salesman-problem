# MTh DAT21 Neural Network Approaches Travelling Salesman Problem - by Tobias Krenn

This is the repository holding code for the Master Thesis

Neural Network-Based Approaches for Solving the Travelling Salesman Problem

A Comparative Analysis of Dense and Convolutional Neural Network Techniques


In a nutshell: 
- Overview on history and methods on the TSP
- Two approaches to solve the TSP with neural nets:
    - Dense NN
    - CNN (TSPs are actually drawn as images)

For further infos or help feel free to contact the author at: tobias.krenn@gmail.com


The project originally is in the following folder-structure (to make the programs work you can also adapt the filenames):

main

- data:

    load_Xy_tsp.py, generate_Xy_tsp_01.py, 
    ALL kind of generated data but models (tsp_solved*.npz, GRY*.npz, BGR*.npz)

- FCN_images: FCN*.py - files
    - models (FCN-models (tf-folders))
    - pix (for image saves)

- NN: NN_predict*.py - files, model_dense*.py - files
    - models: modelNN_*.h5 files
- results: results_eval.py

    - DNN
    - CNN


