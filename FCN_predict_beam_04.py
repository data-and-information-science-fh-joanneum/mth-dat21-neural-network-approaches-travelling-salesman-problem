# loads a CNN model, the raw data and image data from
# filename_model, filename_load, filename_raw
# the -load- and -raw files MUST have the same data, i.e. the -load-file must be generated from the -raw-file!
# predicts results by model and beamsearch (use parameters) and displays them one-by-one
# (see thesis for details on the 5 images)


import numpy as np
import cv2 as cv
from keras.models import load_model
from functions_lib import beamsearch, pathlen


np.set_printoptions(linewidth=180, precision=4)
seed = np.random.randint(999999)
rng = np.random.default_rng(seed=seed)
#################################################################################

#filename_raw =  "../data/tsp_solved_n100x20x20_000.npz"  # use SAME data as in load!!!z
#filename_load = "../data/GRY128_T1cGK_P5_0_LT4_tsp_solved_n100x20x20_000.npz"
#filename_model = "models/modelX_GRY128_T1cGK_P5_0_LT4_tsp_solved_n1000x12x12_000_000.tf"

#filename_raw =  "../data/tsp_solved_n100x12x12_000.npz"  # use SAME data as in load!!!z
#filename_load = "../data/GRY128_T1cGK_P5_0_LT4_tsp_solved_n100x12x12_000.npz"
#filename_model = "models/modelX_GRY128_T1cGK_P5_0_LT4_tsp_solved_n10000x12x12_000_000_e250.tf"

filename_raw =  "../data/tsp_solved_n100x12x12_000.npz"  # use SAME data as in load!!!z
filename_load = "../data/BGR128_T1cRK_P5_5_LT4_tsp_solved_n100x12x12_000.npz"
filename_model = "models/modelX_BGR128_T1cRK_P5_5_LT4_tsp_solved_n1000x12x12_000_001.tf"

filename_save = "pix/pred_D20M6.png"

# beam parameters
beam_start = 1
beam_min = 1
beam_decay = .5
verbose = 1

blur_pred = 0  # gauss-blur the predicted image
disp_speed = -1  # ms, -2 for no display, add save results!!!, -1 for only final result, 0 for debug each step

#dispsize = [5 * 192, 192] # original
dispsize = [5 * 240, 240]
#threshold = 192  # for old mask
pt_size = 3

# load data
print(f"data {filename_raw}")
data = np.load(filename_raw)
# print(list(data.keys()))

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

n_points = data_co.shape[1]
print(f"filename_load: {filename_load}")
img_dim = int(filename_load.split("/")[-1][3:6])  # najo... 128, 192, 256

print(f"RAW: data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")
#####
print(f"loading model {filename_model}, data {filename_load}")
data = np.load(filename_load)

print(f"data {filename_load}")
data = np.load(filename_load)

X_img = data["X"]
X = X_img / 255.0
y = data["y"]  # no division

if X.ndim == 3:
    input_shape = (X.shape[1], X.shape[2], 1)
    X = X[:, :, :, np.newaxis]
elif X.ndim == 4:
    input_shape = (X.shape[1], X.shape[2], 3)
else:
    quit()

print(f"shapes: X: {X.shape} ({X.dtype}), y: {y.shape} ({y.dtype})")

model = load_model(filename_model)
#print(model.summary())

y_pred = model.predict(X)
y_pred = np.squeeze(y_pred)

# stretch gray images
#min0 = np.min(y_pred)
#max0 = np.max(y_pred)
#print(min0, max0)
#y_pred = (y_pred - min0) / (max0 - min0)
#print(y_pred[0, :20, :20])
#print(y_pred.shape, np.unique(y_pred))

y_pred_img = np.round(y_pred * 255).astype(np.uint8)


#quit()


#X = np.squeeze(X)
#X_img = (X * 255).astype(np.uint8)

print(f"shapes: X: {X.shape}, y: {y.shape}, y_pred: {y_pred.shape}")
print(f"shapes: X_img: {X_img.shape} {X_img.dtype}, y: {y.shape} {y.dtype}, y_pred_img: {y_pred_img.shape} {y_pred_img.dtype}")

# make img
if "GRY" in filename_load:
    img = np.zeros((X_img.shape[1], X_img.shape[2] * 5), dtype=np.uint8)
elif "BGR" in filename_load:
    img = np.zeros((X_img.shape[1], X_img.shape[2] * 5, 3), dtype=np.uint8)
else:
    quit()

img_0 = img[:, :X_img.shape[2]]  # X
img_1 = img[:, X_img.shape[2]:X_img.shape[2] * 2]  # y
img_2 = img[:, X_img.shape[2] * 2:X_img.shape[2] * 3]  # prediction from X
img_3 = img[:, X_img.shape[2] * 3:X_img.shape[2] * 4]  # draw all edges, then result (green)
img_4 = img[:, X_img.shape[2] * 4:]  # draw each edge copy from img_1, then final result
img_line = np.zeros_like(img_0)
img_prob = np.zeros_like(img_0)

# prob matrix
pmat = np.zeros((n_points, n_points), dtype=np.float_)

print(f"img shapes: {img.shape} {img_0.shape} {img_1.shape} {img_2.shape} {img_3.shape} ")

#for i in range(3):
for i in range(0, X.shape[0]):
#for i in rng.choice(X.shape[0], X.shape[0], replace=False):
#for i in [94, 79, 44]:
    pmat[:] = 0

    print(f"drawing image {i}:", end=" ")
    if disp_speed >= -1:
        cv.setWindowTitle("Title", f"image {i}: processing...")

    # DEBUG
    #print(f"\nDEBUG img_0.shape {img_0.shape} img_prob.shape: {img_prob.shape} "
    #      f"img_0[:].shape: {img_0[:].shape} X_img[i, :].shape: {X_img[i, :].shape}")

    # issue (with reshape both BGR and GRY work fine)):
    # BGR shapes are (128, 128, 3), (128, 128, 3)
    # GRY shapes are (128, 128, 1), (128, 128)
    img_0[:] = X_img[i, :].reshape(img_0.shape)
    img_1[:] = y[i, :].reshape(img_1.shape)
    img_prob[:] = 255

    if blur_pred == 0:
        img_2[:] = y_pred_img[i, :]
    else:
        img_2[:] = cv.GaussianBlur(y_pred_img[i, :], (blur_pred, blur_pred), 0)

    pt = np.round(data_co[i, :, :] * img_dim).astype(np.int_)

    # compare all possible connections in the complete graph - one by one :-)
    for i1 in range(n_points - 1):
        for i2 in range(i1 + 1, n_points):
            #p1 = np.round(data_co[i, i1, :] * img_dim).astype(np.int_)
            #p2 = np.round(data_co[i, i2, :] * img_dim).astype(np.int_)
            #p1 = points[i1]
            #p2 = points[i2]
            p_dist = ((pt[i1, 0] - pt[i2, 0]) ** 2 + (pt[i1, 1] - pt[i2, 1]) ** 2) ** .5

            # we draw the edge on a empty img_3 (with whatever LINE_... we wish)
            img_3[:] = 255
            cv.line(img_3[:], pt[i1, :], pt[i2, :], color=(0, 0, 0), thickness=1, lineType=cv.LINE_8) # cv.LINE_4, cv.LINE_AA, cv.LINE_8

            # binary_mask3: all not blank pixels in img_3
            binary_mask3 = np.where(img_3[:] != 255)
            # now we copy the area where the line should be (img_3) from the prediction (img_2) to an emptied img_4
            img_4[:] = 255
            img_4[binary_mask3] = img_2[binary_mask3]
            # we sum up the points (greyscale is ok, not only 255-values) from both to compare (line_prob)
            sum3 = np.sum(255 - img_3[:]) + 1 #/ 255  + +1 to prevent div0
            sum4 = np.sum(255 - img_4[:]) + 1 #/ 255
            line_prob = sum4 / sum3
            pmat[i1, i2] = line_prob
            #pmat[n_points - 1 - i1, n_points - 1 - i2] = line_prob  # n_points - 1 kinda does transpose ### WHAT THE FRACK DID I THINK HERE!!!!!!!

            if disp_speed == 0:
                print(f"sum4: {sum4:.2f}, sum3: {sum3:.2f} line_prob: {line_prob:.2f}")
                cv.setWindowTitle("Title", f"image {i}: processing... line_prob: {line_prob:.2f}  ({sum4:.0f}/{sum3:.0f})")
            elif disp_speed > 10:
                cv.setWindowTitle("Title", f"image {i}: processing... line_prob: {line_prob:.2f}")  # same as above

            # draw points for orientation
            """
            for j in range(pt.shape[0]):
                #cv.circle(img_3[:], (pt[j]), 2, (255, 63, 63), -1)  # -1 fills the circle
                #cv.circle(img_4[:], (pt[j]), 2, (255, 63, 63), -1)  # -1 fills the circle
                px, py = pt[j]
                top_left = (px - pt_size, py - pt_size)
                bottom_right = (px + pt_size, py + pt_size)

                cv.rectangle(img_3[:], top_left, bottom_right, (255, 63, 63), -1)  # -1 fills the square
                cv.rectangle(img_4[:], top_left, bottom_right, (255, 63, 63), -1)  # -1 fills the square
            """
            # draw probabilities to img_prob
            cint = round(line_prob * 255)
            cv.line(img_prob, pt[i1, :], pt[i2, :], color=(255 - cint, 255 - cint, 255), thickness=1, lineType=cv.LINE_AA)

            if disp_speed >= 0:
                cv.imshow("Title", cv.resize(img, dsize=dispsize, interpolation=cv.INTER_CUBIC))

            #print(f"points: {p1} {p2} p_dist {p_dist:.2f} sum3:{sum3:.2f} sum4:{sum4:.2f} line_prob: {line_prob:.4f}  i:{i1}-{i2}")
                key = cv.waitKey(disp_speed)
                if key == ord('q') or key == 27:  # 27 is the ASCII value for ESC
                    quit()


    #print(f"pmat:\b{pmat}")
    pmat = pmat + pmat.T  # this mirrors tril to triu - no need for beam (???)
    #print(f"pmat:\b{pmat}")
    beam_path = beamsearch(pmat, start=-1, startwith=None, beam_start=beam_start, beam_min=beam_min, beam_decay=beam_decay, verbose=verbose)
    dm = data_dm[i, :]
    plall = pathlen(beam_path, dm)
    best_ix = np.argsort(plall)

    print(f"o_len: {data_pl[i]:.4f} best_ix {plall[best_ix[0]]:.4f} "
          f"incr: {100 * (plall[best_ix[0]] / data_pl[i] - 1):.2f}% "
          f"beams: {beam_path.shape[0]}")

    # draw best proposal
    img_4[:] = 255
    co_img = np.round(data_co[i, beam_path[best_ix[0]], :] * np.array([X.shape[1], X.shape[1]])).astype(np.int_)
    cv.polylines(img_4[:], [co_img], isClosed=True, color=(0, 63, 0), thickness=1, lineType=cv.LINE_AA)
    #cv.line(img_4[:], co_img[0, :], co_img[-1, :], color=(0, 63, 255), thickness=1, lineType=cv.LINE_AA)

    #print(f"co_img {co_img}")
    # copy prob to img_3
    img_3[:] = img_prob
    # draw points for orientation
    for j in range(pt.shape[0]):
        px, py = pt[j]
        top_left = (px - pt_size, py - pt_size)
        bottom_right = (px + pt_size, py + pt_size)
        cv.rectangle(img_3[:], top_left, bottom_right, (63, 63, 191), -1)  # -1 fills the square
        cv.rectangle(img_4[:], top_left, bottom_right, (63, 191, 63), -1)  # -1 fills the square



    if disp_speed >= -1:
        win_title = f"image {i}: len incr: {100 * (plall[best_ix[0]] / data_pl[i] - 1):.2f}%"
        cv.setWindowTitle("Title", win_title)
        cv.imshow("Title", cv.resize(img, dsize=dispsize, interpolation=cv.INTER_CUBIC))
        key = cv.waitKey(0)
        if key == ord('q') or key == 27:  # 27 is the ASCII value for ESC
            quit()
        elif key == ord('s') and len(filename_save) > 0:
            filename_save_this = f"{filename_save.split('.')[0]}_{i:03}.{filename_save.split('.')[1]}"
            print(f"writing to {filename_save_this}")
            cv.imwrite(filename_save_this, img_2)

        #print(f"### DEBUG ### {key}")

#cv.waitKey(0)
