# makes a predioction file, works just like the predict-program without displaying
# the generated file can be avaluated with 'results.py'


import numpy as np
import cv2 as cv
#import h5py
from keras.models import load_model
from functions_lib import beamsearch, pathlen


np.set_printoptions(linewidth=180, precision=4)
seed = np.random.randint(999999)
rng = np.random.default_rng(seed=seed)
#################################################################################

filename_raw =  "../data/tsp_solved_n100x12x12_000.npz"  # use SAME data as in load!!!
filename_load = "../data/BGR128_T1cRK_P5_5_LT4_tsp_solved_n100x12x12_000.npz"

# iterate models by index
# iterate models by list
filename_model_list = ["models/modelX_BGR128_T1cRK_P5_5_LT4_tsp_solved_n1000x12x12_000_001.tf"]

dosave = True
# beam parameters
beam_start = 3
beam_min = 2
beam_decay = .5
verbose = -1  # 0: each example, 1 ... forward to beamsearch

blur_pred = 0  # gauss-blur the predicted image
#disp_speed = -1  # ms, -2 for no display, add save results!!!, -1 for only final result, 0 for debug each step

# load data
print(f"data {filename_raw}")
data = np.load(filename_raw)
# print(list(data.keys()))

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

n_points = data_co.shape[1]
print(f"filename_load: {filename_load}")
img_dim = int(filename_load.split("/")[-1][3:6])  # najo... 128, 192, 256



print(f"RAW: data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

data = np.load(filename_load)

print(f"data {filename_load}")
data = np.load(filename_load)

X_img = data["X"]
X = X_img / 255.0
y = data["y"]  # no division

# make img
if "GRY" in filename_load:
    img = np.zeros((X_img.shape[1], X_img.shape[2] * 5), dtype=np.uint8)
elif "BGR" in filename_load:
    img = np.zeros((X_img.shape[1], X_img.shape[2] * 5, 3), dtype=np.uint8)
else:
    quit()

img_0 = img[:, :X_img.shape[2]]  # X
img_1 = img[:, X_img.shape[2]:X_img.shape[2] * 2]  # y
img_2 = img[:, X_img.shape[2] * 2:X_img.shape[2] * 3]  # prediction from X
img_3 = img[:, X_img.shape[2] * 3:X_img.shape[2] * 4]  # draw all edges, then result (green)
img_4 = img[:, X_img.shape[2] * 4:]  # draw each edge copy from img_1, then final result
img_line = np.zeros_like(img_0)
img_prob = np.zeros_like(img_0)


for filename_model in filename_model_list:
    print(f"loading model {filename_model}, data {filename_load}")
    filename_result = "../results/res_" + \
                      f"{beam_start}_{beam_min}_{beam_decay:.2f}_{filename_model.split('/')[-1].split('.')[0]}.npz".replace(
                          ".", "", 1)
    # use filename_result2 for cross eval
    filename_result2 = f"{filename_result[:-4]}_{filename_load.split('x')[1]}x{filename_load.split('x')[1]}{filename_result[-4:]}"
    #print(f"shapes: X: {X.shape} ({X.dtype}), y: {y.shape} ({y.dtype})")
    #print(f"  filename_result: {filename_result} {filename_result2} {filename_load.split('x')[1]}")
    #continue

    model = load_model(filename_model)
    #print(model.summary())

    y_pred = model.predict(X)
    y_pred = np.squeeze(y_pred)  # eliminate dims == 1
    y_pred_img = np.round(y_pred * 255).astype(np.uint8)

    n_res = X.shape[0]
    results = np.empty(shape=(n_res, 2), dtype=np.float_)

    # prob matrix
    pmat = np.zeros((n_points, n_points), dtype=np.float_)

    if verbose > 0:
        print(f"  img shapes: {img.shape} {img_0.shape} {img_1.shape} {img_2.shape} {img_3.shape} ")

    for i in range(X.shape[0]):
        pmat[:] = 0

        # issue (with reshape both BGR and GRY work fine)):
        # BGR shapes are (128, 128, 3), (128, 128, 3)
        # GRY shapes are (128, 128, 1), (128, 128)
        img_0[:] = X_img[i, :].reshape(img_0.shape)
        img_1[:] = y[i, :].reshape(img_1.shape)
        img_prob[:] = 255

        if blur_pred == 0:
            img_2[:] = y_pred_img[i, :]
        else:
            img_2[:] = cv.GaussianBlur(y_pred_img[i, :], (blur_pred, blur_pred), 0)

        pt = np.round(data_co[i, :, :] * img_dim).astype(np.int_)

        # compare all possible connections in the complete graph - one by one :-)
        for i1 in range(n_points - 1):
            for i2 in range(i1 + 1, n_points):
                p_dist = ((pt[i1, 0] - pt[i2, 0]) ** 2 + (pt[i1, 1] - pt[i2, 1]) ** 2) ** .5

                # we draw the edge on a empty img_3 (with whatever LINE_... we wish)
                img_3[:] = 255
                cv.line(img_3[:], pt[i1, :], pt[i2, :], color=(0, 0, 0), thickness=1, lineType=cv.LINE_8) # cv.LINE_4, cv.LINE_AA, cv.LINE_8

                # binary_mask3: all not blank pixels in img_3
                binary_mask3 = np.where(img_3[:] != 255)
                # now we copy the area where the line should be (img_3) from the prediction (img_2) to an emptied img_4
                img_4[:] = 255
                img_4[binary_mask3] = img_2[binary_mask3]
                # we sum up the points (greyscale is ok, not only 255-values) from both to compare (line_prob)
                sum3 = np.sum(255 - img_3[:]) + 1  #/ 255  # +1 avoid div0
                sum4 = np.sum(255 - img_4[:]) + 1  #/ 255
                line_prob = sum4 / sum3
                pmat[i1, i2] = line_prob
                #pmat[n_points - 1 - i1, n_points - 1 - i2] = line_prob  # n_points - 1 kinda does transpose ### WHAT THE FRACK DID I THINK HERE!!!!!!!

        #print(f"pmat:\b{pmat}")
        pmat = pmat + pmat.T  # this mirrors tril to triu - no need for beam (???)
        #print(f"pmat:\b{pmat}")
        beam_path = beamsearch(pmat, start=-1, startwith=None, beam_start=beam_start, beam_min=beam_min, beam_decay=beam_decay, verbose=verbose)
        dm = data_dm[i, :]
        plall = pathlen(beam_path, dm)
        best_ix = np.argsort(plall)

        results[i, 0] = data_pl[i]
        results[i, 1] = plall[best_ix[0]]

        if verbose >= 0:
            print(f"  o_len: {data_pl[i]:.4f} best_ix {plall[best_ix[0]]:.4f} "
                  f"incr: {100 * (plall[best_ix[0]] / data_pl[i] - 1):.2f}% "
                  f"beams: {beam_path.shape[0]}")

    if verbose >= 0:
        print(f"\n results:\n{results}")
    #print(filename_model)
    #print(filename_result)
    if dosave:
        print(f"saving results in {filename_result2} (results.shape: {results.shape})")
        np.savez_compressed(filename_result2, results=results)
    else:
        print(f"NOT saving results in {filename_result2} (results.shape: {results.shape})")
        print(np.mean(results, axis=0))


