# create a CNN model
# filename_load from FCN_generate_data_ALL_000.py
# feel free to change parameters and  model structure - watch out to connect the layers properly!


import numpy as np
import os
from sklearn import model_selection

from keras.models import Model
from keras.layers import Input, Dense, Conv2D, Conv2DTranspose, Concatenate, MaxPooling2D, UpSampling2D, Dropout, Flatten, Reshape,\
    LeakyReLU, PReLU, ReLU, ELU
from keras.losses import BinaryCrossentropy, BinaryFocalCrossentropy, CosineSimilarity, CategoricalHinge, KLDivergence, \
    Hinge, LogCosh, Poisson, mse

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")
np.set_printoptions(linewidth=180, precision=4)


# data
filename_load = "../data/BGR128_T1cRK_P5_5_LT4_tsp_solved_n1000x12x12_000.npz" # model 0..5


filename_save_base = f"models/modelX_{filename_load.split('/')[-1][:-4]}"
for version in range(1000):
    filename_save = f"{filename_save_base}_{version:03}.tf"
    if not os.path.exists(filename_save):
        break

#print(filename_load)
#print(filename_save)
#quit()

print(f"data {filename_load}")
data = np.load(filename_load)
#print(list(data.keys()))

X = data["X"] / 255.0
y = (data["y"] / 255.0)#.astype(np.int_)

# debug shrink data
#print(X.shape, y.shape)
#ndata = 5000
#X = X[:ndata, :]
#y = y[:ndata, :]
#print(X.shape, y.shape)
#quit()

# parameters
do_save = True
traintest = .2
epochs = 5
batch_size = 2**4
drop = .05
msf = 16  # model size factor
alpha = 1 - np.mean(y) # determine BFCE alpha, gamma
gamma = 5
leaky = .0
act0 = ReLU()  # "relu" LeakyReLU(alpha=leaky) ReLU(negative_slope=leaky) selu elu linear  # ReLU can be used as LReLU with params
act1 = ReLU()
act2 = act0
actF = "sigmoid"  # linear sigmoid --- softplus tanh, softmax for categories with one hit

pad_con = "same"
pad_dec = "same"
pad_out = "same"
upsint = "bilinear"  # "area", "bicubic", "bilinear", "gaussian", "lanczos3", "lanczos5", "mitchellcubic", "nearest"
optimizer = "adam"
#loss = "binary_crossentropy"  # old one
loss = BinaryFocalCrossentropy(alpha=alpha, gamma=gamma)  # BinaryCrossentropy
metrics = mse  # accuracy

X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=traintest) #, random_state=42
input_shape = X.shape[1:]

print(f"input_shape {input_shape} - X_train.shape:{X_train.shape}, y_train.shape: {y_train.shape} "
      f"X_test.shape:{X_test.shape}, y_test.shape: {y_test.shape}")

# build the model
inputs = Input(shape=input_shape)

# Encoder
conv_e11 = Conv2D(msf, (3, 3), activation=act0, padding=pad_con)(inputs)
conv_e12 = Conv2D(msf, 3, activation=act1, padding=pad_dec)(conv_e11)
conv_e12 = Dropout(drop)(conv_e12)
pool1 = MaxPooling2D(pool_size=(2, 2))(conv_e12)

conv_e21 = Conv2D(msf * 2, (3, 3), activation=act0, padding=pad_con)(pool1)
conv_e22 = Conv2D(msf * 2, 3, activation=act1, padding=pad_dec)(conv_e21)
conv_e22 = Dropout(drop)(conv_e22)
pool2 = MaxPooling2D(pool_size=(2, 2))(conv_e22)

conv_e31 = Conv2D(msf * 4, (3, 3), activation=act0, padding=pad_con)(pool2)
conv_e32 = Conv2D(msf * 4, 3, activation=act1, padding=pad_dec)(conv_e31)
conv_e33 = Conv2D(msf * 4, 3, activation=act2, padding=pad_dec)(conv_e32)
conv_e33 = Dropout(drop)(conv_e33)
pool3 = MaxPooling2D(pool_size=(2, 2))(conv_e33)

conv_e41 = Conv2D(msf * 4, (3, 3), activation=act0, padding=pad_con)(pool3)
conv_e42 = Conv2D(msf * 4, 3, activation=act1, padding=pad_dec)(conv_e41)
conv_e43 = Conv2D(msf * 4, 3, activation=act2, padding=pad_dec)(conv_e42)
conv_e43 = Dropout(drop)(conv_e43)
pool4 = MaxPooling2D(pool_size=(2, 2))(conv_e43)

conv_e51 = Conv2D(msf * 4, (3, 3), activation=act0, padding=pad_con)(pool4)
conv_e52 = Conv2D(msf * 4, 3, activation=act1, padding=pad_dec)(conv_e51)
conv_e53 = Conv2D(msf * 4, 3, activation=act2, padding=pad_dec)(conv_e52)
conv_e53 = Dropout(drop)(conv_e53)
pool5 = MaxPooling2D(pool_size=(2, 2))(conv_e53)

# Bottleneck
conv_e61 = Conv2D(msf * 4, (3, 3), activation=act0, padding=pad_con)(pool5) ### pool5 pool2
conv_e62 = Conv2D(msf * 4, 3, activation=act1, padding=pad_dec)(conv_e61)

# Decoder

convT1 = Conv2DTranspose(msf * 4, (3, 3), activation=act0, padding=pad_dec)(conv_e62)
convT1 = Dropout(drop)(convT1)
concat1 = Concatenate()([pool5, convT1])
up1 = UpSampling2D(size=(2, 2), interpolation=upsint)(concat1)

convT2 = Conv2DTranspose(msf * 4, (3, 3), activation=act0, padding=pad_dec)(up1)
convT2 = Dropout(drop)(convT2)
concat2 = Concatenate()([pool4, convT2])
up2 = UpSampling2D(size=(2, 2), interpolation=upsint)(concat2)

convT3 = Conv2DTranspose(msf * 4, (3, 3), activation=act0, padding=pad_dec)(up2)
convT3 = Dropout(drop)(convT3)
concat3 = Concatenate()([pool3, convT3])
up3 = UpSampling2D(size=(2, 2), interpolation=upsint)(concat3)

convT4 = Conv2DTranspose(msf * 2, (3, 3), activation=act0, padding=pad_dec)(up3)
convT4 = Dropout(drop)(convT4)
concat4 = Concatenate()([pool2, convT4])
up4 = UpSampling2D(size=(2, 2), interpolation=upsint)(concat4)

convT5 = Conv2DTranspose(msf * 1, (3, 3), activation=act0, padding=pad_dec)(up4)
convT5 = Dropout(drop)(convT5)
concat5 = Concatenate()([pool1, convT5])
up5 = UpSampling2D(size=(2, 2), interpolation=upsint)(concat5)


# Output
if "GRY" in filename_load:
    output = Conv2D(1, (3, 3), activation=actF, padding=pad_out)(up5)  #up5  # up2
elif "BGR" in filename_load:
    output = Conv2D(3, (3, 3), activation=actF, padding=pad_out)(up5)  #up5  # up2

# Create the U-Net model
model = Model(inputs=inputs, outputs=output)

print(model.summary())
#quit()  # use this to check valid output-dims

model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=epochs, batch_size=batch_size)

if do_save:
    print(f"saving as {filename_save}")
    #model.save(filename_save)
    model.save(filename_save, save_format="keras")
else:
    print(f"NOT saving as {filename_save}")

# list all data in history
hkeys = list(history.history.keys())
print(f"history.history.keys(): {type(hkeys)} {hkeys} ")
# summarize history for accuracy
fig, ax = plt.subplots(1, 2)
ax[0].plot(history.history[hkeys[1]])
ax[0].plot(history.history["val_" + hkeys[1]])
ax[0].set_title("model " + hkeys[1])
ax[0].set_ylabel(hkeys[1])
ax[0].set_xlabel("epoch")
ax[0].legend(["train", "test"], loc="upper left")

# summarize history for loss
ax[1].plot(history.history[hkeys[0]])
ax[1].plot(history.history[hkeys[2]])
ax[1].set_title("model " + hkeys[0])
ax[1].set_ylabel(hkeys[0])
ax[1].set_xlabel("epoch")
ax[1].legend(["train", "test"], loc="upper left")
plt.show()
