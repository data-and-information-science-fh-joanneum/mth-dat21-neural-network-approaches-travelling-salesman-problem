# loads a result (res*.npz) file
# and makes a nice plot of the predicted vs. optimal TSP pathlengths


import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")


filename_result = "res_3_2_050_modelX_BGR128_T1cRK_P5_5_LT4_tsp_solved_n1000x12x12_000_001_12x12.npz"

if "NN" in filename_result:
    filename_result = "DNN/" + filename_result
elif "GR" in filename_result:
    filename_result = "CNN/" + filename_result


data = np.load(filename_result)
r = data["results"]
#print(r.shape)

col0 = (0, .7, 0)
col1 = (.9, .1, 0)
col2 = (.1, .1, .5)
#col3 = (0, 0, 0)
dark = .75
light = .5
col0d = tuple(np.array(col0) * dark)  # stupid fland... tuples!
col1d = tuple(np.array(col1) * dark)
col2d = tuple(np.array(col2) * dark)
col2l = tuple(1 - (1 - np.array(col2)) * light)
col3 = (.7, .7, .7)

m_l = "H"  # label marker
m_p = "v"  # prediction marker
m_r = "x"  # ratio marker

linestyle0 = "dashed"
linestyle1 = "dotted"
linestyle2 = "solid"

#ps = 10 * 2000 / r.shape[0]  # point_size
ps = 10

avg0 = np.average(r[:, 0])
avg1 = np.average(r[:, 1])
avg2 = np.average(r[:, 1] / r[:, 0])
rat_min = np.min(r[:, 1] / r[:, 0])
rat_max = np.max(r[:, 1] / r[:, 0])

# check equals
rr = np.round(r, decimals=6)
#print(rr)
same = (rr[:, 0] == rr[:, 1])
#print(same)

print(f"filename_result {filename_result}")
print(f"avg: +{-100 + avg2 * 100:.6f}%  avg0: {avg0:.6f} avg1: {avg1:.6f}  avg2: {avg2:.6f} (n={r.shape[0]})")
print(f"std : {np.std(r, axis=0)}")
print(f"gm0: {np.prod(r[:, 0]) ** (1 / r.shape[0]):.6f} gm1: {np.prod(r[:, 1]) ** (1 / r.shape[0]):.6f}")
print(f"perc same: {100 * np.sum(same) / same.shape[0]:.2f}%")


#quit()

# Sort the entire array using the sorted indices
r_sort_ix = np.argsort((r[:, 0]))
r_sort = r[r_sort_ix, :]

fig, ax = plt.subplots(2, 1)  # ax needed for positioning thingies (set_label_coords, ...)
ax[0].scatter(range(r_sort[:, 0].shape[0]), r_sort[:, 0], s=ps, marker=m_l, color=col0, label="label")
ax[0].scatter(range(r_sort[:, 1].shape[0]), r_sort[:, 1], s=ps, marker=m_p, color=col1, label="prediction", alpha=.25)
ax[1].scatter(range(r_sort[:, 0].shape[0]), r_sort[:, 1] / r_sort[:, 0], s=ps, marker=m_r, color=col2,  label="ratio")
ax[1].axhline(y=1, color=col3, linestyle=linestyle2, linewidth=6, zorder=0)  # zorder it back to see the blue dots :-)
ax[0].axhline(y=avg0, color=col0d, linestyle=linestyle0, label="label avg")

ax[0].axhline(y=avg1, color=col1d, linestyle=linestyle0, label="prediction avg")
ax[1].axhline(y=avg2, color=col2d, linestyle=linestyle0, label="ratio avg")
ax[1].axhline(y=rat_min, color=col2, linestyle=linestyle1)
ax[1].axhline(y=rat_max, color=col2, linestyle=linestyle1)

ax[0].annotate(f"{avg0:.4f}", xy=(0, avg0), xytext=(-100, -5), textcoords="offset points", color=col0d)
ax[0].annotate(f"{avg1:.4f}", xy=(0, avg1), xytext=(-100, 5), textcoords="offset points", color=col1d)
ax[1].annotate(f"{avg2:.4f}", xy=(0, avg2), xytext=(-105, 0), textcoords="offset points", color=col2d)

#plt.xlim()
ax[0].set_ylim(np.min(r_sort) * .9, np.max(r_sort) * 1.1)

ax[1].set_xlabel("scenario")
ax[0].set_ylabel("path length")  # labelpad=20)
ax[0].yaxis.set_label_coords(-0.05, 0.9)

ax[1].set_ylabel("ratio")  # labelpad=20)
ax[1].yaxis.set_label_coords(-0.05, 0.9)

ax[0].legend(loc="upper left")
ax[1].legend(loc="upper left")
plt.title("")

plt.show()
