# generates data-files for CNN model generation:
# (filename_list can hold more files)
# the training data from generate_Xy_tsp_01.py is used to
# 'paint' the TSPs to images (features and label)
# according to the settings in parameters, see thesis for details
# example:
#   the parameters can be seen in the name of filename_save
#   filename: '../data/tsp_solved_n1000x50x50_000.npz'
#   filename_save: '../data/GRY128_T1cGK_P5_0_LT4_tsp_solved_n1000x50x50_000.npz'


import numpy as np
import cv2 as cv


np.set_printoptions(linewidth=180, precision=4)
seed = 42  # np.random.randint(999999)
rng = np.random.default_rng(seed=seed)

# parameters
filename_list = ["../data/tsp_solved_n1000x6x6_000.npz",
                 "../data/tsp_solved_n1000x8x8_000.npz",
                 "../data/tsp_solved_n1000x10x10_000.npz",
                 "../data/tsp_solved_n1000x12x12_000.npz",
                 "../data/tsp_solved_n1000x16x16_000.npz",
                 "../data/tsp_solved_n1000x20x20_000.npz"]

filename_list = ["../data/tsp_solved_n1000x50x50_000.npz"]

# parameters:
coldim = 1  # 1: GRY, 3: BGR
imsize = 128  # 128, 192, 256, ...
thickness = 1  # line thickness
lineType = cv.LINE_4  # cv.LINE_4, cv.LINE_AA, cv.LINE_8
psX = 5  # pointsize X
psy = 0  # pointsize y
blur = 0  # gauus-blur size

c_black = (0, 0, 0)
c_g127 = (127, 127, 127)
c_white = (255, 255, 255)
c_red = (0, 0, 255)
c_green = (0, 255, 0)
c_blue = (255, 0, 0)

dosave = True

verbose = 1

col_line_X = c_g127
col_line_y = c_black
col_point_X = c_black
col_point_y = c_white


# gen text for filename from parameters
if coldim == 1:
    colinf = "GRY"
elif coldim == 3:
    colinf = "BGR"
else:
    print("abort ...")
    quit()

t_col = ""
if col_line_X == c_black:
    t_col = t_col + "K"
elif col_line_X == c_g127:
    t_col = t_col + "G"
elif col_line_X == c_white:
    t_col = t_col + "W"
elif col_line_X == c_red:
    t_col = t_col + "R"
elif col_line_X == c_green:
    t_col = t_col + "G"
elif col_line_X == c_blue:
    t_col = t_col + "B"

if col_line_y == c_black:
    t_col = t_col + "K"
elif col_line_y == c_g127:
    t_col = t_col + "G"
elif col_line_y == c_white:
    t_col = t_col + "W"
elif col_line_y == c_red:
    t_col = t_col + "R"
elif col_line_y == c_green:
    t_col = t_col + "G"
elif col_line_y == c_blue:
    t_col = t_col + "B"

if lineType == cv.LINE_4:
    lts = "LT4"
elif lineType == cv.LINE_8:
    lts = "LT8"
elif lineType == cv.LINE_AA:
    lts = "LTA"
else:
    print("abort ...")
    quit()

for filename in filename_list:
    # load data
    data = np.load(filename)
    # print(list(data.keys()))

    data_co = data["co"]
    data_dm = data["dm"]
    data_pa = data["pa"]
    data_pl = data["pl"]
    data_y = data["y"]

    filename_split = filename.split("/")
    filename_split[-1] = f"{colinf}{imsize}_T{thickness}c{t_col}_P{psX}_{psy}_{lts}_" + filename_split[-1]
    filename_save = "/".join(filename_split)
    #print(f"filename_save: {filename_save}")

    if verbose > 0:
        print(f"datafile: {filename}")
        if verbose > 1:
            print(f"  data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
                  f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

    # create empty inages
    X = np.zeros((data_co.shape[0], imsize, imsize, coldim), dtype=np.uint8) + 255
    y = np.zeros((data_co.shape[0], imsize, imsize, coldim), dtype=np.uint8) + 255
    # calc image coordinates (int)
    co_img = data_co * np.array([imsize, imsize])
    co_img = np.round(co_img).astype(np.int_)

    # draw  to all images
    ni = data_co.shape[0]  # shorter name :-)
    nl = data_co.shape[1]

    for i in range(ni):
        # print(f"drawing image {i}: {co_img[i, :].shape}")

        # draw X
        # lines (complete, kuratowsky-graph)
        for p1 in range(nl):
            for p2 in np.setdiff1d([range(nl)], p1):
                cv.line(X[i, :], co_img[i, p1, :], co_img[i, p2, :], color=col_line_X, thickness=thickness,
                        lineType=lineType)
        # points
        if psX > 0:
            for j in range(nl):
                cv.rectangle(X[i, :], co_img[i, j, :] - (psX // 2), co_img[i, j, :] + ((psX + 1) // 2), color=col_point_X,
                             thickness=-1)
                # print(f"debug coo {co_img[i, j, :] - (ps // 2), co_img[i, j, :] + ((ps + 1) // 2)}")

        # draw y: tsp solution (hamilton path, closed)
        cv.polylines(y[i, :], [co_img[i, data_pa[i]]], isClosed=True, color=col_line_y, thickness=thickness,
                     lineType=lineType)
        # points
        if psy > 0:
            for j in range(nl):
                cv.rectangle(y[i, :], co_img[i, j, :] - (psy // 2), co_img[i, j, :] + ((psy + 1) // 2), color=col_point_y,
                             thickness=-1)

        if blur > 0:
            X[i, :] = cv.GaussianBlur(X[i, :], (blur, blur),
                                      0)  # 0 calculates the standard deviation based on the kernel size automatically
            y[i, :] = cv.GaussianBlur(y[i, :], (blur, blur), 0)

    if dosave:
        if verbose > 0:
            print(f"saving data from/to:\n  filename: '{filename}'\n  filename_save: '{filename_save}'")
            np.savez_compressed(filename_save, X=X, y=y)
    else:
        if verbose > 0:
            print(f"NOT saving data from/to:\n  filename: '{filename}\n  filename_save: '{filename_save}'")
