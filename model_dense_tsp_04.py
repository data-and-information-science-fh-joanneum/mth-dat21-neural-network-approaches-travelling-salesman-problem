# generate a DNN model from a tsp_solved-file (generated with generate_Xy_tsp_01.py)
# filename = "../data/tsp_solved_n100x50x50_000.npz" - put training-data-file here
# feel free to play with parameters and model structure
# it saves a file if 'dosave', if filename exists it increase the counter in the name, so no overwriting
# ('weighted_binary_crossentropy' is experimental and not used)
# after completeing the model the loos/metrics are displayed with pyplot - save it if you like it :-)


import numpy as np
import os
from keras.models import Sequential
from keras.layers import Dense, Flatten, Dropout
from keras.activations import linear, relu, elu, selu, leaky_relu, tanh, relu6, sigmoid, softmax
from keras.losses import BinaryCrossentropy, BinaryFocalCrossentropy, CosineSimilarity, CategoricalHinge, KLDivergence, Hinge, LogCosh, Poisson

from keras import regularizers

from sklearn import model_selection
import tensorflow as tf
from sklearn.utils.class_weight import compute_class_weight

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")

np.set_printoptions(linewidth=180, precision=4)


def weighted_binary_crossentropy(y_true, y_pred):
      #print(f"y_true {y_true}")
      #print(f"y_pred {y_pred}")
      # Assuming y_pred is the output of your model's sigmoid activation
      epsilon = tf.constant(1e-10)  # Small value to avoid log(0)

      y_true = tf.cast(y_true, tf.float32)
      y_pred = tf.clip_by_value(y_pred, epsilon, 1 - epsilon)

      # Calculate the binary cross-entropy loss
      bce = -(y_true * tf.math.log(y_pred) + (1 - y_true) * tf.math.log(1 - y_pred))

      # Apply class weights
      weighted_bce = tf.where(y_true == 1, class_weights[1] * bce, class_weights[0] * bce)

      return tf.reduce_mean(weighted_bce)


def custom_loss1(y_true, y_pred):
      y_true = tf.cast(y_true, tf.float32)
      # MSE part
      mse = tf.reduce_mean(tf.square(y_true - y_pred))

      # Penalty for non-sparse predictions
      # This encourages the network to produce outputs close to 0 or 1
      sparsity_penalty = tf.reduce_mean(tf.abs(y_pred - 0.5))

      return mse + 1. * sparsity_penalty


filename = "../data/tsp_solved_n100x50x50_000.npz"
filename_save_base = "models/modelNN_" + (filename.split("/")[-1]).split(".")[0]  # version will be added before saving

data = np.load(filename)
print(f"data.keys: {list(data.keys())}")

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

print(f"data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")
X_dim = data_dm.shape[-1]
print(f"X_dim: {X_dim}")
X = data_dm[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]
#y = data_y.reshape(data_y.shape[0], -1)
y = data_y[:, np.tril_indices(X_dim, k=-1)[0], np.tril_indices(X_dim, k=-1)[1]]  ## WTF!!!

print(f"X.shape:{X.shape}, y.shape: {y.shape}")
X_train, X_test, y_train, y_test = model_selection.train_test_split(X, y, test_size=0.2) #, random_state=42

# parameters
epochs = 1
batch_size = 2**6
drop = .05
l1 = 0#1e-7
l2 = 1e-6
act0 = elu  # relu elu selu PReLU linear leaky_relu...
act1 = elu  # or imported not "": linear, relu, elu, selu, sigmoid, leaky_relu, tanh, relu6, ...
# act1 = lambda x: relu(x, alpha=0.1)  # strange...
actF = "sigmoid"  # sigmoid (softmax for categories with one hit)
optimizer = "adam"
# calc params
alpha = 1 / (1 + (X_dim - 3) / 2)  # share of ones in label (10 cities: 10/45)
gamma = 5  # default: 2, 0:BinaryCrossentropy, 5 NICE! (3, 10: worse than 5, 50: catastrophe)
loss = BinaryFocalCrossentropy(alpha=alpha, gamma=gamma)  # alt: BinaryCrossentropy()
metrics = ["mse"]  # accuracy? stupid?

#loss = Poisson()
#loss = ["mse"] # DO NOT USE THAT ONE: MeanSquaredError() (loading/eval error - keras still experimental ;-))
#metrics = FocalLoss()

dosave = False

#print(f"data_dm[0, :]\n{data_dm[0, :]}\nX[0, :] {X[0, :].shape}\n{X[0, :]}")
#print(f"data_y[0, :]\n{data_y[0, :]}\ny[0, :]  {y[0, :].shape}\n{y[0, :]}")

# Define the model
d_0 = (X_dim * (X_dim - 1)) // 2  # 45 (n=10)
d_1 = X_dim ** 2  # 100 (n=10)

print(f"d_0: {d_0}, d_1: {d_1}")

model = Sequential()
#, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2)
model.add(Dense(d_0 * 1, activation=act0, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), input_shape=(X.shape[-1],)))
model.add(Dense(d_0 * 1, activation=act1, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2)))
model.add(Dropout(drop))
###if l1 == 0 and l2 == 0: model.add(Dropout(drop))

#
for _ in range(4):
      model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
      model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
      model.add(Dropout(drop))
      #
"""
model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))

model.add(Dense(d_0 * 4, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 4, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))
"""
"""
model.add(Dense(d_0 * 6, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 6, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))

model.add(Dense(d_0 * 8, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 8, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))

model.add(Dense(d_0 * 6, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 6, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))

model.add(Dense(d_0 * 4, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 4, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))
"""
"""
model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 2, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))
"""


model.add(Dense(d_0 * 1, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act0))
model.add(Dense(d_0 * 1, kernel_regularizer=regularizers.L1L2(l1=l1, l2=l2), bias_regularizer=regularizers.L1L2(l1=l1, l2=l2), activation=act1))
model.add(Dropout(drop))

model.add(Dense(y.shape[-1], activation=actF))


# Compile the model
model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

# Calculate class weights
#class_weights = compute_class_weight("balanced", classes=np.unique(y_train.ravel()), y=y_train.ravel())
#print(f"class_weights: {class_weights}")
#model.compile(optimizer=optimizer, loss=custom_loss1, metrics=metrics)

print(model.summary())

# Train the model
history = model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=epochs, batch_size=batch_size)
pred = model.predict(X_test)

for version in range(1000):
      filename_save = f"{filename_save_base}_{version:03}.h5"
      if not os.path.exists(filename_save):
            break

if dosave:
      print(f"saving as {filename_save}")
      model.save(filename_save)
else:
      print(f"NOT saving as {filename_save}")

# list all data in history
hkeys = list(history.history.keys())
print(f"history.history.keys(): {type(hkeys)} {hkeys} ")
# summarize history for accuracy
fig, ax = plt.subplots(1, 2)
ax[0].plot(history.history[hkeys[1]])
ax[0].plot(history.history["val_" + hkeys[1]])
ax[0].set_title("validation metric: " + hkeys[1])
ax[0].set_ylabel(hkeys[1])
ax[0].set_xlabel("epoch")
ax[0].legend(["train", "test"], loc="upper left")

# summarize history for loss
ax[1].plot(history.history[hkeys[0]])
ax[1].plot(history.history[hkeys[2]])
ax[1].set_title("" + hkeys[0])
ax[1].set_ylabel(hkeys[0])
ax[1].set_xlabel("epoch")
ax[1].legend(["train", "test"], loc="upper left")
plt.show()
