# library containing functions to calculate  and conduct the beamsearch


import numpy as np


def pathlen(pa, dm):
    """
    calculate pathlength of hamilton path, ends at start point
    Args:
        pa: list or 1d-ndarray integers, tour-points
        dm: distance matrix of shape pa.shape**2

    Returns: float, len of hamilton path

    """
    if pa.ndim == 1:
        pa = pa[np.newaxis, :]
    # print(f"pa.ndim={pa.ndim}, dm.ndim={dm.ndim}")
    return np.sum(dm[pa[:, :-1], pa[:, 1:]], axis=-1) + dm[pa[:, -1], pa[:, 0]]


def beamsearch(p, start=-1, beam_start=3, beam_min=2, beam_decay=1, startwith=None, verbose=2):
    """
    calculate hamilton paths with beam search
    Args:
        p: ndarray (float), n x n matrix (symmetric) with probabilities of edges of graph
        start: int, optional, starting point
        beam: int, nr of beams starting at each node/point
            (for n=10, beam=4 fast (24576 paths); n=20, beam=3 takes forever, beam=2 ok)
        startwith: int, optional, if not None the returned paths all are rolled so they start with this point
        verbose: 0, 1, 2, 3: different levels of "chattiness" of output info

    Returns: ndarray (int), dim = n x nr of created patch

    """

    class Node:
        def __init__(self, point, pathsofar=None):
            self.point = point
            self.pathsofar = pathsofar

    def roll1d(arr, roll):
        """
        roll a 2d array's rows by a individual amount roll
        Args:
            arr: 2d ndarr
            roll: 1d ndarr of size arr.shape[0]

        Returns: 2d ndarray - fancy-rolled
        """

        roll = np.array(roll)  # just to be sure
        row_ix = np.arange(arr.shape[0]).reshape(-1, 1)
        col_ix = np.arange(arr.shape[1])
        roll[roll < 0] += arr.shape[1]  # avoid negatives
        col_ix = col_ix - roll[:, np.newaxis]

        return arr[row_ix, col_ix]

    n = p.shape[0]

    if start < 0 or start > (n - 1):
        # find rows that contain largest values in p
        start = np.argpartition(np.max(p, axis=-1), -3)[::-1][:beam_start]
    else:
        start = [start]

    anchors = [Node(point=i, pathsofar=[]) for i in start]
    nodes = [anchors]
    beam = beam_start

    if verbose > 0:
        print(f"startingpoint: {start}")
        print(f"start {start}")

    # count beams
    n_beams = len(anchors)

    # iterate over all levels
    for level in range(n - 1):
        newnodes = []
        # iterate over all nodes from the previous level
        for prevnode in nodes[-1]:
            prevpoint = prevnode.point
            prevpath = prevnode.pathsofar

            if verbose > 1:
                print(f"  prevnode: {prevpath} {prevpoint}")

            newpathsofar = prevpath + [prevpoint]  # add this point to path

            ix_row = prevpoint
            ix_col = np.delete(np.arange(n), newpathsofar)

            # generate new array p0 where to search for max items

            p0 = p[ix_row, ix_col]
            beam = max(beam_start - level * beam_decay, beam_min)
            searchlen = min(round(beam), n - level - 1)  # limit with maximum remaining node possibilities

            argpart = np.argpartition(p0, -searchlen)[-searchlen:]
            newnodes_nr = ix_col[argpart]
            if verbose > 2:
                print(p0)
            if verbose > 1:
                print(f"    newnodes_nr: {newnodes_nr} argpart: {argpart}")

            for b in range(searchlen):
                # add a new node
                newpoint = newnodes_nr[b]
                newnode = Node(point=newpoint, pathsofar=newpathsofar)
                newnodes.append(newnode)
                if verbose > 1:
                    print(f"    newnode: {newpoint}: {newpathsofar}   ix_row: {ix_row} ix_col: {ix_col} ")

        n_beams = n_beams * searchlen

        nodes.append(newnodes)
        if verbose > 0:
            print(f"### calculating level {level}, startnodes: {len(nodes[-1])}, beam: {beam:.2f}, "
                  f"searchlen: {searchlen}, p0: {p0.shape} --- n_beams: {n_beams}")

    final_paths = np.array([(node.pathsofar + [node.point])for node in nodes[-1]])
    if verbose > 1:
        print(final_paths)

    if startwith is not None:
        # here we roll each row so that it starts with startwith (when given only)
        final_paths = roll1d(final_paths, -np.where(final_paths == startwith)[-1])

    return final_paths