# view a file generated with 'generate_Xy_tsp_01.py'
# use 'n' ta acess an individual TSP in the file


import numpy as np

filename = "tsp_solved_n100x6x6_000.npz"
data = np.load(filename)

print(f"\ndata.keys(): {list(data.keys())}")

data_co = data["co"]
data_dm = data["dm"]
data_pa = data["pa"]
data_pl = data["pl"]
data_y = data["y"]

print(f"data_co.shape:{data_co.shape}, data_dm.shape: {data_dm.shape}, data_pa.shape: {data_pa.shape}, "
      f"data_pl.shape: {data_pl.shape}, data_y.shape: {data_y.shape}")

n = 13
print(f"data_co:\n{data_co[n, :]}\ndata_dm:\n{data_dm[n, :]}\ndata_pa: {data_pa[n]}\n"
      f"data_pl: {data_pl[n]}\ndata_y:\n{data_y[n, :]}")
